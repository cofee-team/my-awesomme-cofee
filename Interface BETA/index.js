const express = require(`express`);
const app = express();
const http = require(`http`).createServer(app);
const path = require(`path`);
const io = require(`socket.io`)(http);
const _ = require("underscore");
var log=require(`./log`)

const fs = require('fs');
let fichier = fs.readFileSync(__dirname + '/BDDINFO.weebconfig');
let configfile = JSON.parse(fichier);

const MongoClient = require('mongodb').MongoClient;
const url = configfile.url;
const dbname = configfile.dbname;

const client = new MongoClient(url);

client.connect(function(err) {
    console.log("Connected successfully to server");
  
    const db = client.db(dbname);
    console.log("Loggedin")
  });

  
const port = process.env.PORT || 3000;

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
  app.use(express.json());

//////////////////////////////////////////////// CODE BETA///////////////////////////////////////////////////////////////
var listServeur= Array();
var listnetoserveur=Array();
var historique=Array();
var win=0;
var lose=0;
ListServeur();
winLose();
function NewServeur(socket){
    listServeur.push(socket.id);
    listnetoserveur[socket.id]= Array();
    historique[socket.id]=Array();
}
function FindServeurListenner(socket){
    return listnetoserveur[socket.id];
}
function AddServeurListenner(id,socketlisten){
    return listnetoserveur[id].push(socketlisten);
}
async function ListServeur(){
    var test=[];
    try{
        
          await client.connect(async function(err) {
            const db = client.db(dbname);
            const docs = await db.collection("DataPartie").find({}).sort( [['_id', -1]] ).limit(20)
            var retour= [];
            await docs.forEach(async function(value,index,arr){
                retour =retour.concat([value.id])
                historique[value.id]=value.historique
            },retour)
            listServeur= _.union(listServeur,retour)

          })

    }
    catch(e){
        console.log(e)
    }
    return test;
}
function SendMessageToListenner(socket,message){
    if(_.isUndefined(listnetoserveur[socket.id].array)) return;
    listnetoserveur[socket.id].array.forEach(element => {
        element.emit("FromServer",message);
    });
}
function RemoveServeurListener(socket){
    listnetoserveur.forEach(function(value,key){
        listnetoserveur[key] = _.without(value,socket.id);
    });
}
function Gethistorique(id){
    if(_.isUndefined(historique[id])||_.isEmpty(historique[id])){

        return "EMPTY"
    }
    return historique[id]
}
function AddToHistorique(id,stringue){
    
    historique[id].push("{"+(String(stringue)).replace(`""`, `"`).replace(`"}`,`}`)+"}");
}
async function save(socket,bool){
    try{
        client.connect(async function(err) {
            const db = client.db(dbname);
    
            await db.collection("DataPartie").insertOne({id:socket.id,win:bool,historique:historique[socket.id]})
            console.log("j'envoie des donnée")
          })
    }
    catch(e){
        console.log(e)
    }
}
async function winLose(){
    try{
        await client.connect(async function(err) {
          const db = client.db(dbname);
          const docs = await db.collection("DataPartie").find({"win":true}).toArray()
          win=docs.length
           const docs2 = await db.collection("DataPartie").find({"win":false}).toArray()
          lose=docs2.length
        })
  }
  catch(e){
      console.log(e)
  }
}


////////////////////////////////////////////////// CODE PLUS PROPRE /////////////////////////////////////////////////////////////////

app.use(express.static(path.join(__dirname, `client/build`)));
app.get('/api/nbrpartie',(req,res)=>{
    var ip = req.headers[`x-forwarded-for`] || req.connection.remoteAddress;
    log.info(`${ip}: /api/nbrpartie ${win+lose} `)
    res.json(win+lose);
})
app.get(`/api/win`,(req,res)=>{
    var ip = req.headers[`x-forwarded-for`] || req.connection.remoteAddress;
    log.info(`${ip}: /api/win ${win} `)
    res.json(win);
})
app.get(`/api/lose`,(req,res)=>{
    var ip = req.headers[`x-forwarded-for`] || req.connection.remoteAddress;
    log.info(`${ip}: /api/lose ${lose}`)
    res.json(lose);
})
app.get(`/api/getlistpartie`,async (req,res)=>{
    var ip = req.headers[`x-forwarded-for`] || req.connection.remoteAddress;
    log.info(`${ip}: /api/getlistpartie `)
    await ListServeur();
    res.json(listServeur);
});
app.post(`/api/historique`,(req,res)=>{
    var ip = req.headers[`x-forwarded-for`] || req.connection.remoteAddress;
    if(_.isUndefined(req.body.ID)) {
        log.info(`${ip}: /api/historique with 404 not id provided `)
        return res.status(404).send("No id provided")
    }
    var id = req.body.ID;
    if(Gethistorique(id)=="EMPTY"){
        log.info(`${ip}: /api/historique with 404 Server not found to the id ${id}`)
        return res.status(404).send("Server not found")
    }
    log.info(`${ip}: /api/historique with 200 and acces the history of ${id}`)
    console.log(JSON.stringify(Gethistorique(id)))
    res.send(JSON.stringify(Gethistorique(id)));
})

  app.get(`*`, (req,res) =>{
    var ip = req.headers[`x-forwarded-for`] || req.connection.remoteAddress;
    log.info(`${ip}: has been directed to the react Client `)
    res.sendFile(path.join(__dirname+`/client/build/index.html`));
});

io.on(`connection`, (socket) => {
    console.log("A user is connected");
    log.info(`A user is connected with the socket ${socket.id}`)
    socket.on(`infofrompartie`, (msg) => {
      console.log(`message: ` + msg+` ID:`+socket.id);
      log.info(`${socket.id} send to infofrompartie ${msg}`)
      AddToHistorique(socket.id,msg);
      SendMessageToListenner(socket,msg);
    });
    socket.on("serveur",()=>{
        console.log(`serveur: ` + socket.id);
        log.info(`${socket.id} is a server`)
        NewServeur(socket);
        socket.emit("serveur",socket.id);
    })
    socket.on("listento",(msg)=>{
        console.log(`message: ` + msg+` ID:`+socket.id);
        console.log(msg);
        log.info(`${socket.id} ask to listen to ${msg}`)
        AddServeurListenner(msg,socket);
    })
    socket.on("Resultat",(msg)=>{
        console.log(msg)
        if(msg=='"true"') {
            win++
            save(socket,true);
        }
        else{
            lose++
            save(socket,false);
        }
        
    })
    socket.on("disconnect",()=>{
        log.info(`${socket.id} is disconnected`)
        console.log(`Disconnect: `+socket.id);
        RemoveServeurListener(socket);
    })
    
  });



http.listen(port);


console.log(`App is listening on port ` + port);

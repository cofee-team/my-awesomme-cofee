import React from 'react';
import _ from 'underscore';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faWater,faTree,faArrowLeft,faArrowRight,faCoffee, faMugHot } from '@fortawesome/free-solid-svg-icons'
var where = "https://myawesomecofee.herokuapp.com"



class Partie extends React.Component{
    constructor(props){
        super(props)
        this.state={
            Partie:[],
            test:[],
            Turn:2,
            update:[]
          }
          var tab = new Array(10);
          for(var indeX = 0;indeX<tab.length;indeX++){
              tab[indeX] = new Array(10);
              for(var indeY=0;indeY<10;indeY++){
                tab[indeX][indeY]="a";
              }
          }
          this.state.update=tab;
          this.Handleleft = this.Handleleft.bind(this);
          this.HandleRight = this.HandleRight.bind(this);
          this.do = this.do.bind(this);
          this.undo = this.undo.bind(this);
    }
    do(who){
        console.log(who)
        var swap = this.state.update;
        swap[this.state.Partie[who].Turn.cooX][this.state.Partie[who].Turn.cooY] =this.state.Partie[who].Turn.serveurquijoue?"s":"c";
        this.setState({update:swap})

    }
    undo(who){
        console.log(who)
        var swap = this.state.update;
        swap[this.state.Partie[who].Turn.cooX][this.state.Partie[who].Turn.cooY] ="a";
        this.setState({update:swap})
    }
    Handleleft(){
        if(this.state.Turn<2) return
        this.setState({Turn:(this.state.Turn-1)})
        this.undo(this.state.Turn-2)

    }
    HandleRight(){
        if(this.state.Turn===this.state.Partie.length) return
        this.setState({Turn:(this.state.Turn+1)})
        this.do(this.state.Turn-1)
    }

    componentDidMount(){
        fetch(where+'/api/historique',{method:'post',headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },body:JSON.stringify({ID:this.props.path.match.params.id})})
        .then(res=>res.json())
        .then((data)=>{
            data.forEach(async element => {
                await this.setState(state=>{
                    const Partie =[...state.Partie,JSON.parse(element)]
                    return {
                        ...state,
                        Partie:Partie,
                    }
                })
            });
          
        }).catch((res)=>this.setState({test:"Serveur non existant ou introuvable pour le moment" + res}))
      }

    render(){
        var retour;
        var json;
        var X =-1;
        var Y=-1;
        if(!_.isUndefined(this.state.Partie[0])){
            json=this.state.Partie[0];

             retour = json.RepresSymbolique.map(function(item,i){
                X+=1;
                var entry=item.map(function(element,j){
                    Y+=1;
                    return(<td style={styles[element]} id={X*10+Y}key={j} className={element}>{element==="w"?<FontAwesomeIcon icon={faWater}/>:element==="Z"?<FontAwesomeIcon icon={faTree}/> :this.state.update[X][Y]==="a"?element:this.state.update[X][Y]==="c"?<FontAwesomeIcon style={{color:"black"}} icon={faCoffee}/>:<FontAwesomeIcon style={{color:"black"}} icon={faMugHot}/>}</td>);
                },this);
                Y=-1;
                return(<tr key={i}><td>{X} </td >{entry}</tr>)
            },this)
            X=0;
        }
        
        return(
            <div id="display">
                <h1> Partie {this.props.path.match.params.id} </h1>
                <div className="Plateau">
                <p>{this.state.test}</p>
                <table style={{}}>
                    <tbody> 
                        <tr><td key="0 "> </td><td key="00">0</td><td key="10">1</td><td key="20">2</td><td key="30">3</td><td key="40">4</td><td key="50">5</td><td key="60">6</td><td key="70">7</td><td key="80">8</td><td key="90">9</td></tr>
                        {retour}
                    </tbody>
                </table>
                </div>
                <div >
                <button onClick={this.Handleleft} style={{marginLeft:"50px",marginRight:"50px"}} className="left"><FontAwesomeIcon icon={faArrowLeft}/></button>
                <p style={{display:"inline-block"}}>Tour: {this.state.Turn-1}</p>
                <button onClick={this.HandleRight} style={{marginLeft:"50px",marginRight:"50px"}} className="right"><FontAwesomeIcon icon={faArrowRight}/></button>
                {this.state.Partie.map(function(d,idx){
                    if(X<=this.state.Turn-2){X+=1; return}
                    X+=1;
                    return(<p id={"Step"+X}className="step" key={Math.random()}>{JSON.stringify(d)}</p>)
                  },this)}
                </div>
            </div>)
    }

}
var styles={
    a:{
        backgroundColor: '#'+Math.floor(Math.random()*16777215).toString(16),
      },
      b:{
      backgroundColor: '#'+Math.floor(Math.random()*16777215).toString(16),
      },
      c:{
        backgroundColor: '#'+Math.floor(Math.random()*16777215).toString(16),
      },
      d:{
        backgroundColor: '#'+Math.floor(Math.random()*16777215).toString(16),
      },
      e:{
        backgroundColor:'#'+Math.floor(Math.random()*16777215).toString(16) ,
      },
      f:{
        backgroundColor: '#'+Math.floor(Math.random()*16777215).toString(16),
      },
      g:{
        backgroundColor:'#'+Math.floor(Math.random()*16777215).toString(16),
      },
     h:{
        backgroundColor: '#'+Math.floor(Math.random()*16777215).toString(16),
      },
      i:{
        backgroundColor: '#'+Math.floor(Math.random()*16777215).toString(16),
      },
      j:{
        backgroundColor: '#'+Math.floor(Math.random()*16777215).toString(16),
      },
      k:{
        backgroundColor: '#'+Math.floor(Math.random()*16777215).toString(16),
      },
      l:{
        backgroundColor: '#'+Math.floor(Math.random()*16777215).toString(16),
      },
      m:{
        backgroundColor: '#'+Math.floor(Math.random()*16777215).toString(16),
      },
      n:{
        backgroundColor: '#'+Math.floor(Math.random()*16777215).toString(16),
      },
      o:{
        backgroundColor: '#'+Math.floor(Math.random()*16777215).toString(16),
      },
      p:{
        backgroundColor: '#'+Math.floor(Math.random()*16777215).toString(16),
      },
      q:{
        backgroundColor: '#'+Math.floor(Math.random()*16777215).toString(16),
      }
}




export default Partie;

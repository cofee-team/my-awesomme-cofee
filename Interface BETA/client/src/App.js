import React, { Suspense } from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Partie from './Display'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoffee, faMugHot } from '@fortawesome/free-solid-svg-icons'
var where = "https://myawesomecofee.herokuapp.com"


class App extends React.Component{
  constructor(props){
    super(props)
    this.state={
      Partie:[],
      WIN:[],
      LOSE:[],
      status:""
    }
  }

  componentDidMount(){
    fetch(where+'/api/getlistpartie')
    .then(res=>res.json())
    .then((data)=>{
      const swap = data.reverse();
      this.setState({Partie:data})
      console.log(swap);
    }).catch(console.log)
    fetch(where+'/api/win')
    .then(res=>res.json())
    .then((data)=>{
      this.setState({WIN:data})
    }).catch(console.log)
    fetch(where+'/api/lose')
    .then(res=>res.json())
    .then((data)=>{
      this.setState({LOSE:data})
    }).catch((data)=>{
      console.log(data)
      this.setState({status:"Veuillez verifiez votre connection internet"})
    })
  }

  render(){
    return(
      <Router>
      <div className="topnav" id="myTopnav">
        <a href="/ " className="active">Bataille du café</a>
        
        <a href="/" style={{float:"right"}}>Win <FontAwesomeIcon style={{color:"white"}} icon={faCoffee}/>:{this.state.WIN}/Lose <FontAwesomeIcon style={{color:"white"}} icon={faMugHot}/>:{this.state.LOSE}</a>
      </div>
      <div className="content">
      <h1 style={{textAlign:"center",color:"red"}}>{this.state.status}</h1>
      <Suspense fallback={<h1>Loading....</h1>}/>
      <Switch>
        {this.state.status===""&&<Route path="/" exact>
        <div id="list">
        <h1>List des partie identifier par leur Socket.id</h1>
        {this.state.Partie.map(function(d,idx){return (<a href={d} key={Math.random()} className="server"><p  id={d}>{d}</p></a>)})}
        </div>
        </Route>}
        <Route path="/:id" render={(location)=><Partie path={location} />} />
      </Switch>
      </div>
      <div className="footer">
        <div style={{lineHeight:"0.001",textAlign:"center",alignContent:"middle",fontSize:"x-small",margin:"0 auto",}}>
            <p style={{lineHeight:"0.001"}}>Made with</p>
            <p style={{lineHeight:"0.001"}}>{"< 3 and too much "}<FontAwesomeIcon icon={faCoffee}/></p>
            <p style={{lineHeight:"0.001"}}>By batleforc <a href="https://gitlab.com/cofee-team/my-awesomme-cofee" style={{color:"white"}}> {">here on git<"}</a></p> 
          </div>
        </div>
      </Router>
    )
  }
}

export default App;

using System;
using System.Collections.Generic;

namespace Biblio
{
    public static class Terrain //uml http://www.plantuml.com/plantuml/umla/TSx1oi8m30VmUvyYn-z3tq2HWGSHF8Zi9Y9P5sOXwsRI3cJitQsk1n7CxKz-_v120qg8BHlBg0ebYQ3peu4qYoqgdPSNAB5YmfYNC3OKO9g9lQ5yaDqBaBv95WUdONr4iSHC6-0KhtsEC4A2A-Z53HW8D8NlooVgXVJHLXsxU_nLcJj3vshOUnVogtyQnFa6X6pyDvCfoDUnvIS0
    {
        public static CASE[,] TableauTerrain { get; } = new CASE[10, 10];
        public static char[,] TableauGraines { get; } = new char[10, 10];
        public static List<Parcelle> list_parcelle { get; private set; }
        public static char[,] Represymbolique { get; private set; }
        
        /// <summary>
        /// Fonction d'initialisation et d'attribution des valeur a partir de la String représentant la map
        /// Initialise les case, les liste de parcelle et la representation symbolique.
        /// </summary>
        /// <param name="recu">Chaine de charactére comportant les information de chaque Case</param>
        public static void InitTerrain(string recu)
        {
            string[] decouper = recu.Split('|');
            string[][] swap = new string[decouper.Length - 1][];
            for (int index = 0; index < decouper.Length - 1; index++)
                swap[index] = decouper[index].Split(':');

            for (int index = 0; index < 10; index++)
                for (int indey = 0; indey < 10; indey++)
                {
                    int.TryParse(swap[index][indey], out int valeurCase);
                    TableauTerrain[index, indey] = new CASE(valeurCase, index, indey);
                    TableauGraines[index, indey] = 'N';
                }
            Traitement_Parcelle.BoucleTraitement(TableauTerrain);
            list_parcelle = Traitement_Parcelle.Parcelles;
            Represymbolique = Traitement_Parcelle.Represymbolique;
        }
    }
}
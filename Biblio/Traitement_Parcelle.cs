﻿using System;
using System.Collections.Generic;

namespace Biblio
{
    public static  class Traitement_Parcelle //umlhttp://www.plantuml.com/plantuml/umla/TO_FIiH038VlynGvjbYVG5dMFzuyYEvjAHAdGGFJcPfa2gNqtHrRYbvsO209-NryEQgXMEw3ym5LuIJ8HZr5Utz1yHG2pGxAk_QeT6xXH59MuZgxybyevwQ5Lng4TEe_Kk3lJ3339zaD3C8Z6e7-DiiMUMQrsnrzWBtI2vajD2Rku3vb7-XFi5gDchRU_5RmmztRqw7e-2ogd6B5qPhow_-WHzOXu5JLRd57Ybsv_WS0
    {
        private static CASE[,] terrain;
        public static char[,] Represymbolique { get; private set; } = new char[10, 10];
        public static List<Parcelle> Parcelles { get; private set; } = new List<Parcelle>();

        /// <summary>
        /// Boucle de traitement avec pour objectif de TROUVER une case non assigner et de lancer Recurssion et d'attribuer son retour a la liste des parcelle ainsi que de remplir le tableau de Representation Symbolique des parcelle
        /// /!\ Attention representation symbolique a une uttiliter importante pour l'IA
        /// </summary>
        public static void BoucleTraitement(CASE[,] p_terrain)
        {
            terrain = p_terrain;
            char id_parcelle = 'a'; //ascii code 97
            for (int indeX = 0; indeX < 10; indeX++)
                for (int indeY = 0; indeY < 10; indeY++)
                    if (!terrain[indeX, indeY].treated)
                        if (terrain[indeX, indeY].IsTERRE)
                            Parcelles.Add(new Parcelle(Recurssion(indeX, indeY), id_parcelle++));
                        else
                            Represymbolique[indeX, indeY] = terrain[indeX, indeY].EAU ? 'w' : 'Z';
            Parcelles.ForEach(delegate (Parcelle SwapParcelle) { SwapParcelle.Cases.ForEach(delegate (CASE swapCase) { Represymbolique[swapCase.coox, swapCase.cooy] = SwapParcelle.id_parcelle; }); });
        }

        /// <summary>
        /// A partir d'une case non traiter ajoute toute les autre case de la parcelle dans une liste 
        /// </summary>
        /// <param name="p_x">Coordonée en X du point non traiter</param>
        /// <param name="p_y">Coordonée en Y du point non traiter</param>
        /// <returns>Liste de case constituant une parcelle unique</returns>
        private static List<CASE> Recurssion(int p_x, int p_y)
        {
            terrain[p_x, p_y].treated = true;
            List<CASE> ListeCaseParcelle = new List<CASE>() { terrain[p_x, p_y] };
            if ((p_x - 1 >= 0) && terrain[p_x - 1, p_y].IsTreatable && !terrain[p_x, p_y].NORD)
                ListeCaseParcelle.AddRange(Recurssion(p_x - 1, p_y));
            if ((p_x + 1 < 10) && terrain[p_x + 1, p_y].IsTreatable && !terrain[p_x, p_y].SUD)
                ListeCaseParcelle.AddRange(Recurssion(p_x + 1, p_y));
            if ((p_y + 1 < 10) && terrain[p_x, p_y + 1].IsTreatable && !terrain[p_x, p_y].EST)
                ListeCaseParcelle.AddRange(Recurssion(p_x, p_y + 1));
            if ((p_y - 1 >= 0) && terrain[p_x, p_y - 1].IsTreatable && !terrain[p_x, p_y].OUEST)
                ListeCaseParcelle.AddRange(Recurssion(p_x, p_y - 1));

            return ListeCaseParcelle;
        }

        /// <summary>
        /// Affiche la représentation symbolique
        /// </summary>
        public static void Display()
        {
            Console.WriteLine();
            for (int iteration = 0; iteration < 10; iteration++)
            {
                for (int iteration2 = 0; iteration2 < 10; iteration2++)
                {
                    Console.Write(Represymbolique[iteration, iteration2] + "|");
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}
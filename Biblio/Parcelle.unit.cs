using System;
using System.Collections.Generic;
using System.Linq;

namespace Biblio
{
    public class Parcelle //uml: http://www.plantuml.com/plantuml/umla/NO_1IiGm48RlUOevTj4tK6HbhoA5FQYdaat6xa0wKoRJYYxxG3w7BsP2HRlcC4m-_e7_ssRpQjEGN4W-PsYz1am9JmxAkmf7hq2n6t_mGXyes-tEPxo3K6Q64q2FTWEZqkmD8TVFsIrfOeF7WqvCXd-abJBtg3DE-e_k4Y7R0hCfSGzFH-d5XjNzdZwmQozg7KGIu33QUoTlZEeMF0j5k8ynY6WKvgzFR6fvA59-s5o6Kb6hJitASPsh15w9OrsQMcJiWiZpDVpkBnjtTblaE0tf6m00
    {
        //variable
        public char id_parcelle { get; set; }
        public List<CASE> Cases {  get; private set; } = new List<CASE>();

        //propriete
        public int Nbrunite => Cases.Count;
        public int PointServeur => Cases.Where<CASE>(var => var.owned.ToLower() == "s").Count();
        public int PointClient => Cases.Where<CASE>(var => var.owned.ToLower() == "c").Count();
        public string Whogotme => PointClient > PointServeur ? "c" : PointClient == PointServeur?"n": "s";
        public int Size_Parcelle => Cases.Count();
        public bool empty_owner => Cases.Where<CASE>(var => var.owned.ToLower() != "n").Count() == 0;

        //function
        public void Addcoordonée(CASE param) => Cases.Add(param);
        public void AddListCase(List<CASE> param) => Cases.AddRange(param);
        public CASE findCase(int p_cooX, int p_cooY) => Cases.Find(p_case => p_case.coox == p_cooX && p_case.cooy == p_cooY);

        public Parcelle(List<CASE> cases, char p_idparcelle)
        {
            Cases = cases ?? throw new ArgumentNullException(nameof(Cases));
            id_parcelle = p_idparcelle;
        }

        public Parcelle(char p_idparcelle)
        {
            id_parcelle = p_idparcelle;
        }
    }
}
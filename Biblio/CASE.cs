﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Biblio
{
    public class CASE //UML : http://www.plantuml.com/plantuml/uml/TP11Qm8n48Nl-olcQ2dz1Rqe6c4FxS9k5dgDPf21D2F9Q17nlpTne4IAEGoyzxvv95bcSKbEtw7R1vSph5t6QmVbl58KM3D_mHMEYSvE43BAEnpgk3qmboUCGZlc0DjXDBRHPZMtyJ2RgOsdUTEen-np63VjBlz4z8sMXEKYwlpP0dssPXnD0nQdecuNKDqiYU81-jM7zh_Lz_MHn5wEkAY2egTzQJ-b5oNEJFwER1D78Kp_EUssFDtR5xgeexjrIuo-_Fyl
    {

        //coordonée du point
        public int coox { private set; get; } = 0;
        public int cooy { private set; get; } = 0;

        //Getter de statut pour la case
        public bool FORET { set; get; } = false;
        public bool EAU { set; get; } = false;
        public bool IsTERRE => IAM.ToLower() == "terre";

        public bool OUEST { set; get; } = false;
        public bool SUD { set; get; } = false;
        public bool EST { set; get; } = false;
        public bool NORD { set; get; } = false;

        public string owned { set; get; } = "N";
        public bool treated { set; get; } = false;
        public bool IsTreatable => IsTERRE && !treated;

        /// <summary>
        /// Constructeur de la classe C permettant d'instancier ces paramétre a partir d'un entier indicateur et de ces coordonée
        /// </summary>
        /// <param name="indicateur"> Indicateur de paramétre</param>
        /// <param name="x">coordonée X sur le plateau</param>
        /// <param name="y">coordonée Y sur le plateau</param>
        public CASE(int indicateur, int x, int y)
        {
            cooy = y;
            coox = x;
            InitFrontiere(InitType(indicateur));
        }

        private int InitType(int indicateur)
        {
            EAU = indicateur >= 64;
            indicateur = (indicateur >= 64) ? indicateur - 64 : indicateur;
            FORET = indicateur >= 32;
            return (indicateur >= 32) ? indicateur - 32 : indicateur;
        }

        private void InitFrontiere(int indicateur)
        {
            EST = indicateur >= 8;
            indicateur = (indicateur >= 8) ? indicateur - 8 : indicateur;
            SUD = indicateur >= 4;
            indicateur = (indicateur >= 4) ? indicateur - 4 : indicateur;
            OUEST = indicateur >= 2;
            indicateur = (indicateur >= 2) ? indicateur - 2 : indicateur;
            NORD = indicateur >= 1;
        }

        /// <summary>
        /// Fonction qui retourne le type de case
        /// </summary>
        /// <returns>chaine de string</returns>
        public string IAM => FORET ? "foret" : EAU ? "eau" : "terre";


        public override string ToString()
        {
            return string.Format("CooX:{0} \nCooY:{1} \n  NORD: {2}\nEST:{3}    OUEST:{4}\nSUD:{5}",coox,cooy,NORD,OUEST,EST,SUD);
        }
    }
}
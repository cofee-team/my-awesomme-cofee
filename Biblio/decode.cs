﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Biblio
{
    public class decode //uml: http://www.plantuml.com/plantuml/umla/TT31Ii0m30RWUvuYnmrVOcnCy3HHy28UkZQCGar6cmbZxDsjj8A3sLk__8GaNL8JTViC03QOb10TMN5q0ERtWFEkzFw1aUo61owaY6iK9QlaCENlMO99e-S5TLy9uKAbd_jhkU7h6hoMNutF8W47OVN4EhAL6txqx6_upOJ_kB_n9_uo_U-KdZMFOqMQjaAH5qhF9kPyzbAefnWjIwHXcXeioVPQvfzrcxfy2oS0TCGkd_WR
    {
        public decode(byte[] p_recu) => recu = p_recu ?? throw new ArgumentNullException(nameof(recu));

        //variable
        public byte[] recu { get; protected set; }
        public string type => decoded != "" ? SplitString.Length==1?"string":"int": "null";

        //propriete
        public string decoded => recu==null?"": Encoding.ASCII.GetString(recu, 0, recu.Length).Trim();
        public string[] SplitString => decoded.Split(':');
        public bool ContientEnco => decoded.Contains("ENCO");
        public bool ContientFini => decoded.Contains("FINI");
        public bool ContientVali => decoded.Contains("VALI");
        public bool ContientValiB => decoded.Contains("VALIB");
        public bool contientInvaB => decoded.Contains("INVAB");
        

        public int[] COO()
        {
            if (decoded == null)
                return new int[0];
            if (ContientValiB)
            {

                Int32.TryParse(SplitString[1].Split("ENCO")[0], out int swap2);
                return new int[2] { swap2 / 10, swap2 - ((int)(swap2 / 10) * 10) };
            }
            if (SplitString.Length == 3)
            {
                return new int[5];
            }
            if (SplitString[1].Contains("ENCO"))
            {
                string swapString = SplitString[1].Replace("ENCO","").Trim();
                Int32.TryParse(swapString, out int swap1);
                return new int[2] { swap1 / 10, swap1 - ((int)(swap1 / 10) * 10) };
            }
            Int32.TryParse(SplitString[1], out int swap);
            return new int[2] { swap / 10, swap - ((int)(swap / 10) * 10) };
        }
        public int[] ResPartie()
        {
            if (decoded == null)
                return new int[0];
            if (SplitString.Length <=2)
            {
                return new int[5];
            }
            if (decoded.Contains("FINIS")&& SplitString.Length > 3)
            {
                int.TryParse(SplitString[2], out int swap3);
                int.TryParse(SplitString[3], out int swap4);
                return new int[2] { swap3, swap4 };
            }
            int.TryParse(SplitString[1], out int swap);
            int.TryParse(SplitString[2], out int swap2);
            return new int[2] { swap, swap2 };
        }

        public static byte[] EncodeCOO(int[] coo) => Encoding.ASCII.GetBytes($"A:{coo[0]}{coo[1]}");
        public static byte[] Encode(string stringswap) => Encoding.ASCII.GetBytes(stringswap);
    }
}

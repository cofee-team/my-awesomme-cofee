using Biblio;
using System;
using System.Collections.Generic;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using SocketIOClient;
using System.Threading.Tasks;

namespace PlayBot
{
    public static class Interface
    {
        static SocketIO client = new SocketIO("https://myawesomecofee.herokuapp.com/");
        public static string ID { get; private set; }
        public static bool status = false;
        public async static Task INIT()
        {
            try
            {
                client.On("serveur", response =>
                {
                    ID = response.GetValue<string>();
                    Console.WriteLine(ID);
                });
                await client.ConnectAsync();
                await client.EmitAsync("serveur");
                status = true;
            }
            catch(Exception ee)
            {
                Console.WriteLine(ee.Message);
                status = false;
            }
            Console.WriteLine("EUH");
            
        }

        public static void SendInfoStartPartie()
        {
            DisplayRepresSymbolique();
        }
        public static void DisplayRepresSymbolique()
        {
            if (!status) return;
            string jsonstring = JsonConvert.SerializeObject(IALogique.represymbolique);
            client.EmitAsync("infofrompartie", $"\"RepresSymbolique\":{jsonstring }".Replace("\"\"", "").Replace("\\", ""));
        }

        public static void DisplayScore(int[] coo)
        {
            WinLose(coo[0] > coo[1] ? true : coo[0] == coo[1] ? true : false) ;
            if (!status) return;
            string jsonstring = JsonConvert.SerializeObject($"\"Score_Client\": {coo[0]}, \"Score_Serveur\": {coo[1]}");
            client.EmitAsync("infofrompartie", $"\"Score\":{"{"+ jsonstring+"}"}".Replace("\"\"", "").Replace("\\", ""));
        }
        public static void WinLose(bool resultat)
        {
            if (!status) return;
            string jsonstring = JsonConvert.SerializeObject(resultat.ToString().ToLower());
            client.EmitAsync("Resultat", $"{jsonstring}".Replace("\"\"", "").Replace("\\", ""));
        }
        public static void DisplayUnTour(bool ServeurQuiJoue, int[] coo)
        {
            if (!status) return;
            string jsonstring = JsonConvert.SerializeObject($"\"serveurquijoue\":{ServeurQuiJoue.ToString().ToLower()},\"cooX\":{coo[0]},\"cooY\":{coo[1]}");
            client.EmitAsync("infofrompartie", $"\"Turn\":{ "{"+ jsonstring+"}" }".Replace("\"\"", "").Replace("\\", ""));
        }
        public static void Valide(bool Vali)
        {
            if (!status) return;
            string jsonstring = JsonConvert.SerializeObject(Vali);
            client.EmitAsync("infofrompartie", $"\"vali\":{ "{"+ jsonstring+"}" }".Replace("\"\"", "").Replace("\\", ""));
        }
    }
}

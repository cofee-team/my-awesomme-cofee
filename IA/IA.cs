using System;
using Biblio;
using System.Collections.Generic;

namespace PlayBot
{
    public static class IALogique
    {
        private static List<Parcelle> listparcelle;
        public static char[,] represymbolique { get; private set; }
        public static char[,] tableauGraines { get; set; } = new char[10, 10];
        public static CASE[,] tableauTerrain { get; private set; }
        public static int[,] powerCase { get; private set; } = new int[10, 10];
        public static int[] cooDernierPointPlacer { get; private set; } = new int[2];
        private static Random random = new Random();

        #region INIT
        /// <summary>
        /// initialise les valeur de la classe IA
        /// </summary>
        /// <param name="p_Represymbolique"></param>
        /// <param name="p_TableauTerrain"></param>
        /// <param name="p_list_parcell"></param>
        public static void INIT(char[,] p_Represymbolique,CASE[,] p_TableauTerrain,List<Parcelle> p_list_parcell)
        {
            listparcelle = p_list_parcell ?? throw new ArgumentNullException(nameof(listparcelle));
            represymbolique = p_Represymbolique ?? throw new ArgumentNullException(nameof(represymbolique));
            tableauTerrain = p_TableauTerrain ?? throw new ArgumentNullException(nameof(tableauTerrain));
        }
        /// <summary>
        /// Réinitialise chaque case du tableau a 0
        /// </summary>
        private static void InitTab() 
        {
            for (int indeX = 0; indeX < 10; indeX++)
                for (int indeY = 0; indeY < 10; indeY++)
                    powerCase[indeX, indeY] = 0;
        }
        #endregion

        #region Traitement
        /// <summary>
        /// Boucle de traitement qui permet de réattribuer les power case
        /// </summary>
        /// <param name="p_coodernierpointplacer">Tableau avec en 0 les coordonée en X et en 1 Y</param>
        public static void BoucleTraitement(int[] p_coodernierpointplacer)
        {
            InitTab();
            cooDernierPointPlacer = p_coodernierpointplacer ?? throw new ArgumentNullException(nameof(cooDernierPointPlacer));
            for(int indeX = 0; indeX < 10; indeX++)
            {
                if (indeX != p_coodernierpointplacer[0]&&GetParcelle(indeX, p_coodernierpointplacer[1])!=null)
                    powerCase[indeX, p_coodernierpointplacer[1]] = PointCase(indeX, p_coodernierpointplacer[1]);
                if (indeX != p_coodernierpointplacer[1]&&(GetParcelle(p_coodernierpointplacer[0], indeX)!=null))
                    powerCase[p_coodernierpointplacer[0], indeX] = PointCase(p_coodernierpointplacer[0], indeX);
            }
        }

        /// <summary>
        /// Définis le score d'une case
        /// </summary>
        /// <param name="p_cooX">coordonée en X </param>
        /// <param name="p_cooY">coordonée en Y </param>
        /// <returns></returns>
        private static int PointCase(int p_cooX,int p_cooY)
        {
            if (CaseDefOwned(p_cooX, p_cooY) || PointONSameParcelle(p_cooX, p_cooY)) return -1; 
            //if (PointServeurParcelleDefOwned(p_cooX, p_cooY)) return 0;
            int retour = ParcelleEmpty(p_cooX, p_cooY);
            retour += PointClientPointServeur(p_cooX, p_cooY);
            retour += PointClientGotAlftheParcelle(p_cooX, p_cooY);
            retour += CasegotSiblingAround(p_cooX, p_cooY);
            return (retour);
        }

        /// <summary>
        /// Permet de récupérer une parcelle
        /// </summary>
        /// <param name="p_cooX">coordonée en X </param>
        /// <param name="p_cooY">coordonée en Y </param>
        /// <returns></returns>
        private static Parcelle GetParcelle(int p_cooX, int p_cooY) => listparcelle.Find(Parcelle => Parcelle.findCase(p_cooX, p_cooY) != null);
        /// <summary>
        /// Test si la parcelle ne contient pas de graine
        /// </summary>
        /// <param name="p_cooX">coordonée en X </param>
        /// <param name="p_cooY">coordonée en Y </param>
        /// <returns></returns>
        private static int ParcelleEmpty(int p_cooX, int p_cooY) => GetParcelle(p_cooX, p_cooY).empty_owner ? 1 : 0;
        /// <summary>
        /// Test si personne ne posséde la parselle
        /// </summary>
        /// <param name="p_cooX">coordonée en X </param>
        /// <param name="p_cooY">coordonée en Y </param>
        /// <returns></returns>
        private static int PointClientPointServeur(int p_cooX, int p_cooY) => GetParcelle(p_cooX, p_cooY).Whogotme == "n" ? 1 : 0;
        /// <summary>
        /// Test si le client posséde la moitier des point de la parcelle
        /// </summary>
        /// <param name="p_cooX">coordonée en X </param>
        /// <param name="p_cooY">coordonée en Y </param>
        /// <returns></returns>
        private static int PointClientGotAlftheParcelle(int p_cooX, int p_cooY) => (GetParcelle(p_cooX, p_cooY).PointClient + 1) >= (GetParcelle(p_cooX, p_cooY).Nbrunite / 2) ? 1 : 0;

        /// <summary>
        /// Test si notre case posséde des case posséder par le client autour
        /// </summary>
        /// <param name="p_cooX">coordonée en X </param>
        /// <param name="p_cooY">coordonée en Y </param>
        /// <returns></returns>
        private static int CasegotSiblingAround(int p_cooX, int p_cooY)
        {
            int retour = p_cooY+1<10?tableauGraines[p_cooX, p_cooY + 1] == 'c' ? 1 : 0:0;
            retour += p_cooY - 1 > 0 ? tableauGraines[p_cooX, p_cooY - 1] == 'c' ? 1 : 0:0;
            retour += p_cooX + 1 < 10 ? tableauGraines[p_cooX+1, p_cooY] == 'c' ? 1 : 0:0;
            return (retour += p_cooX - 1 > 0 ? tableauGraines[p_cooX-1, p_cooY] == 'c' ? 1 : 0:0)%3;
        }

        /// <summary>
        /// Test si la parcelle est posséder par le serveur
        /// </summary>
        /// <param name="p_cooX"></param>
        /// <param name="p_cooY"></param>
        /// <returns></returns>
        private static bool PointServeurParcelleDefOwned(int p_cooX, int p_cooY) => GetParcelle(p_cooX, p_cooY).PointServeur >= (GetParcelle(p_cooX, p_cooY).Nbrunite / 2);
        /// <summary>
        /// test si la case est posséder par le serveur
        /// </summary>
        /// <param name="p_cooX">coordonée en X </param>
        /// <param name="p_cooY">coordonée en Y </param>
        /// <returns></returns>
        private static bool CaseDefOwned(int p_cooX, int p_cooY) => (tableauGraines[p_cooX, p_cooY] == 's'|| tableauGraines[p_cooX, p_cooY] == 'c') ? true : false;
        /// <summary>
        /// Test si la case tester est sur la meme parcelle que le point précédement jouer
        /// </summary>
        /// <param name="p_cooX">Coo en X du point tester</param>
        /// <param name="p_cooY">Coo en Y du point tester</param>
        /// <returns></returns>
        private static bool PointONSameParcelle(int p_cooX, int p_cooY) => GetParcelle(cooDernierPointPlacer[0], cooDernierPointPlacer[1]) == null ? false : GetParcelle(cooDernierPointPlacer[0], cooDernierPointPlacer[1]).findCase(p_cooX, p_cooY) != null ? true : false;
  
        #endregion

        #region Coordonnée a jouer
        /// <summary>
        /// Extrait la liste des Case avec le score le plus élever
        /// </summary>
        /// <returns></returns>
        private static List<CASE> GetListCaseTop()
        {
            List<CASE> retour = new List<CASE>();
            int max = 0;
            for(int indeX = 0; indeX < 10; indeX++)
            {
                if (powerCase[indeX, cooDernierPointPlacer[1]] == max)
                    retour.Add(tableauTerrain[indeX, cooDernierPointPlacer[1]]);
                else if (powerCase[indeX, cooDernierPointPlacer[1]] > max)
                {
                    max = powerCase[indeX, cooDernierPointPlacer[1]];
                    retour.Clear();
                    retour.Add(tableauTerrain[indeX, cooDernierPointPlacer[1]]);
                }
                if (powerCase[cooDernierPointPlacer[0], indeX] == max)
                    retour.Add(tableauTerrain[cooDernierPointPlacer[0], indeX]);
                else if (powerCase[cooDernierPointPlacer[0], indeX] > max)
                {
                    max = powerCase[cooDernierPointPlacer[0], indeX];
                    retour.Clear();
                    retour.Add(tableauTerrain[cooDernierPointPlacer[0], indeX]);
                }
            } 
            return retour;
        }
        /// <summary>
        /// Génére des coordonée random quand aucune case na un score supérieur a 0
        /// </summary>
        /// <returns> Les coordonnée du point a jouer</returns>
        private static int[] GetCasewhenzero() => (random.Next() % 2) == 1? new int[2] { cooDernierPointPlacer[0], random.Next() % 10 }: new int[2] { random.Next() % 10, cooDernierPointPlacer[1] };
        /// <summary>
        /// Sélectionne une des coordonnée random dans la liste
        /// </summary>
        /// <param name="p_listCase"> Liste case issue de GetListCaseTop</param>
        /// <returns> retourne des coordonnée de point</returns>
        private static int[] GetcasefromList(List<CASE> p_listCase)
        {
            int rand = random.Next() % p_listCase.Count;
            return new int[2] { p_listCase[rand].coox, p_listCase[rand].cooy };
        }

        /// <summary>
        /// Renvoie les coordonnée d'un point a jouer a partir des info précédente
        /// </summary>
        /// <returns></returns>
        public static int[] GetCase()
        {
            List<CASE> listCase = GetListCaseTop();
            if (listCase.Count == 0) return GetCasewhenzero();
            if (listCase.Count == 1) return new int[2] { listCase[0].coox, listCase[0].cooy };
            return GetcasefromList(listCase);
        }

        /// <summary>
        /// permet de commencer en selectionnant une des parcelle les plus petiteS
        /// </summary>
        public static int[] PremierTour => new int[2] { listparcelle.Find(Parcelle => Parcelle.Nbrunite == 2).Cases[0].coox, listparcelle.Find(Parcelle => Parcelle.Nbrunite == 2).Cases[0].cooy };
    #endregion
}
}

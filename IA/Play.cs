using System;
using System.Collections.Generic;
using System.Text;
using Biblio;
using System.Threading.Tasks;

namespace PlayBot
{
    public static class Play
    {
        public static connexion Connex = new connexion("localhost", "1213");
        public static decode deco;
        public static async Task StartPartie()
        {

            Console.WriteLine("Connecting to the UI...");
            deco = new decode(Connex.Connect());
            await Interface.INIT();
            Console.WriteLine("Pour l'interface rendez vous sur https://myawesomecofee.herokuapp.com/");
            Console.WriteLine($"Etes vous pret ? {(Interface.status ? $"Votre code pour retrouver votre partie :{Interface.ID}" : "")} ");
            Terrain.InitTerrain(deco.decoded);
            IALogique.INIT(Terrain.Represymbolique, Terrain.TableauTerrain, Terrain.list_parcelle);
            Interface.DisplayRepresSymbolique();
            Console.ReadKey();
            Traitement_Parcelle.Display();
            Connex.Send(decode.EncodeCOO(IALogique.PremierTour));
            DisplayTurn(false, IALogique.PremierTour);
            deco = new decode(Connex.Receive());
            Console.WriteLine(deco.decoded);
            if (deco.ContientVali)
            {
                Console.WriteLine("Le serveur valide le placement");
                PlacementPion(false, IALogique.PremierTour[0], IALogique.PremierTour[1]);
            }
            else
            {
                Console.WriteLine("Le serveur refuse le placement");
            }
            int boucle = 0;
            bool BoocleContinue = TurnFromServeur();
            while (BoocleContinue)
            {
                //DisplayPowerCase();
                PlayTurn();
                //DisplayOwned();
                boucle += 1;
                //Console.WriteLine(boucle);
                BoocleContinue = TurnFromServeur();
            }
            DisplayScore();
        }

        public static void PlayTurn()
        {
            int[] played = IALogique.GetCase();
            Connex.Send(decode.EncodeCOO(played));
            DisplayTurn(false, played);
            deco = new decode(Connex.Receive());
            if (deco.ContientVali)
            {
                Console.WriteLine("Le serveur valide le placement");
                PlacementPion(false, played[0], played[1]);
            }
            else
            {
                Console.WriteLine("Le serveur refuse le placement");
                Console.WriteLine(deco.decoded);
            }
        }
        public static bool TurnFromServeur()
        {
            if (!deco.ContientValiB&&!deco.contientInvaB)
                deco = new decode(Connex.Receive());
            if (deco.ContientFini) return false;
            if (deco.COO() == new int[5] || deco.COO() == new int[0])
                deco = new decode(Connex.Receive());
            PlacementPion(true, deco.COO()[0], deco.COO()[1]);
            IALogique.BoucleTraitement(deco.COO());
            DisplayTurn(true, deco.COO());
            if (deco.ContientEnco) return true;
            deco = new decode(Connex.Receive());
            if (deco.ContientEnco) return true;
            return false;
        }

        public static void DisplayTurn(bool serveurquijoue, int[] coo)
        {
            Console.WriteLine($"{(serveurquijoue ? "Le serveur a jouer" : "Le client a jouer")} COOX: {coo[0]} COOY:{coo[1]}");
            Interface.DisplayUnTour(serveurquijoue, coo);
        }
        public static void DisplayPowerCase()
        {
            Console.WriteLine("PowerCase:");
            for (int indeX = 0; indeX < 10; indeX++)
            {
                for (int indeY = 0; indeY < 10; indeY++)
                    Console.Write(IALogique.powerCase[indeX, indeY]);
                Console.WriteLine();
            }
        }
        public static void DisplayOwned()
        {
            Console.WriteLine("Tableau Graines");
            for (int indeX = 0; indeX < 10; indeX++)
            {
                for (int indeY = 0; indeY < 10; indeY++)
                    Console.Write(Terrain.TableauGraines[indeX, indeY]);
                Console.Write("    ");
                for (int indeY = 0; indeY < 10; indeY++)
                    Console.Write(IALogique.tableauGraines[indeX, indeY]);
                Console.WriteLine();
            }
        }
        public static void DisplayScore()
        {
            if (!deco.decoded.Contains("FINIS"))
                deco = new decode(Connex.Receive());
            if(deco.ResPartie()==new int[0]|| deco.ResPartie() == new int[5])
                deco = new decode(Connex.Receive());
            Console.WriteLine(deco.decoded);
            Console.WriteLine($"Score Client: {deco.ResPartie()[0]} Score Serveur: {deco.ResPartie()[1]}");
            Interface.DisplayScore(deco.ResPartie());
            if (deco.ResPartie()[0] > deco.ResPartie()[1])
                Console.WriteLine(@"
                                                                                                                                               
                                                                                                                                               
YYYYYYY       YYYYYYY     OOOOOOOOO     UUUUUUUU     UUUUUUUU     WWWWWWWW                           WWWWWWWWIIIIIIIIIINNNNNNNN        NNNNNNNN
Y:::::Y       Y:::::Y   OO:::::::::OO   U::::::U     U::::::U     W::::::W                           W::::::WI::::::::IN:::::::N       N::::::N
Y:::::Y       Y:::::Y OO:::::::::::::OO U::::::U     U::::::U     W::::::W                           W::::::WI::::::::IN::::::::N      N::::::N
Y::::::Y     Y::::::YO:::::::OOO:::::::OUU:::::U     U:::::UU     W::::::W                           W::::::WII::::::IIN:::::::::N     N::::::N
YYY:::::Y   Y:::::YYYO::::::O   O::::::O U:::::U     U:::::U       W:::::W           WWWWW           W:::::W   I::::I  N::::::::::N    N::::::N
   Y:::::Y Y:::::Y   O:::::O     O:::::O U:::::D     D:::::U        W:::::W         W:::::W         W:::::W    I::::I  N:::::::::::N   N::::::N
    Y:::::Y:::::Y    O:::::O     O:::::O U:::::D     D:::::U         W:::::W       W:::::::W       W:::::W     I::::I  N:::::::N::::N  N::::::N
     Y:::::::::Y     O:::::O     O:::::O U:::::D     D:::::U          W:::::W     W:::::::::W     W:::::W      I::::I  N::::::N N::::N N::::::N
      Y:::::::Y      O:::::O     O:::::O U:::::D     D:::::U           W:::::W   W:::::W:::::W   W:::::W       I::::I  N::::::N  N::::N:::::::N
       Y:::::Y       O:::::O     O:::::O U:::::D     D:::::U            W:::::W W:::::W W:::::W W:::::W        I::::I  N::::::N   N:::::::::::N
       Y:::::Y       O:::::O     O:::::O U:::::D     D:::::U             W:::::W:::::W   W:::::W:::::W         I::::I  N::::::N    N::::::::::N
       Y:::::Y       O::::::O   O::::::O U::::::U   U::::::U              W:::::::::W     W:::::::::W          I::::I  N::::::N     N:::::::::N
       Y:::::Y       O:::::::OOO:::::::O U:::::::UUU:::::::U               W:::::::W       W:::::::W         II::::::IIN::::::N      N::::::::N
    YYYY:::::YYYY     OO:::::::::::::OO   UU:::::::::::::UU                 W:::::W         W:::::W          I::::::::IN::::::N       N:::::::N
    Y:::::::::::Y       OO:::::::::OO       UU:::::::::UU                    W:::W           W:::W           I::::::::IN::::::N        N::::::N
    YYYYYYYYYYYYY         OOOOOOOOO           UUUUUUUUU                       WWW             WWW            IIIIIIIIIINNNNNNNN         NNNNNNN
                                                                                                                                               
                                                                                                                                               
                                                                                                                                               
                                                                                                                                               
                                                                                                                                               
                                                                                                                                               
                                                                                                                                               
");
            else if (deco.ResPartie()[0] < deco.ResPartie()[1])
                Console.WriteLine(@"
                                                                                                                                                      
                                                                                                                                                      
YYYYYYY       YYYYYYY     OOOOOOOOO     UUUUUUUU     UUUUUUUU     LLLLLLLLLLL                  OOOOOOOOO        SSSSSSSSSSSSSSS EEEEEEEEEEEEEEEEEEEEEE
Y:::::Y       Y:::::Y   OO:::::::::OO   U::::::U     U::::::U     L:::::::::L                OO:::::::::OO    SS:::::::::::::::SE::::::::::::::::::::E
Y:::::Y       Y:::::Y OO:::::::::::::OO U::::::U     U::::::U     L:::::::::L              OO:::::::::::::OO S:::::SSSSSS::::::SE::::::::::::::::::::E
Y::::::Y     Y::::::YO:::::::OOO:::::::OUU:::::U     U:::::UU     LL:::::::LL             O:::::::OOO:::::::OS:::::S     SSSSSSSEE::::::EEEEEEEEE::::E
YYY:::::Y   Y:::::YYYO::::::O   O::::::O U:::::U     U:::::U        L:::::L               O::::::O   O::::::OS:::::S              E:::::E       EEEEEE
   Y:::::Y Y:::::Y   O:::::O     O:::::O U:::::D     D:::::U        L:::::L               O:::::O     O:::::OS:::::S              E:::::E             
    Y:::::Y:::::Y    O:::::O     O:::::O U:::::D     D:::::U        L:::::L               O:::::O     O:::::O S::::SSSS           E::::::EEEEEEEEEE   
     Y:::::::::Y     O:::::O     O:::::O U:::::D     D:::::U        L:::::L               O:::::O     O:::::O  SS::::::SSSSS      E:::::::::::::::E   
      Y:::::::Y      O:::::O     O:::::O U:::::D     D:::::U        L:::::L               O:::::O     O:::::O    SSS::::::::SS    E:::::::::::::::E   
       Y:::::Y       O:::::O     O:::::O U:::::D     D:::::U        L:::::L               O:::::O     O:::::O       SSSSSS::::S   E::::::EEEEEEEEEE   
       Y:::::Y       O:::::O     O:::::O U:::::D     D:::::U        L:::::L               O:::::O     O:::::O            S:::::S  E:::::E             
       Y:::::Y       O::::::O   O::::::O U::::::U   U::::::U        L:::::L         LLLLLLO::::::O   O::::::O            S:::::S  E:::::E       EEEEEE
       Y:::::Y       O:::::::OOO:::::::O U:::::::UUU:::::::U      LL:::::::LLLLLLLLL:::::LO:::::::OOO:::::::OSSSSSSS     S:::::SEE::::::EEEEEEEE:::::E
    YYYY:::::YYYY     OO:::::::::::::OO   UU:::::::::::::UU       L::::::::::::::::::::::L OO:::::::::::::OO S::::::SSSSSS:::::SE::::::::::::::::::::E
    Y:::::::::::Y       OO:::::::::OO       UU:::::::::UU         L::::::::::::::::::::::L   OO:::::::::OO   S:::::::::::::::SS E::::::::::::::::::::E
    YYYYYYYYYYYYY         OOOOOOOOO           UUUUUUUUU           LLLLLLLLLLLLLLLLLLLLLLLL     OOOOOOOOO      SSSSSSSSSSSSSSS   EEEEEEEEEEEEEEEEEEEEEE
                                                                                                                                                      
                                                                                                                                                      
                                                                                                                                                      
                                                                                                                                                      
                                                                                                                                                      
                                                                                                                                                      
                                                                                                                                                      
");
            Console.WriteLine(@"



TTTTTTTTTTTTTTTTTTTTTTTHHHHHHHHH     HHHHHHHHHEEEEEEEEEEEEEEEEEEEEEE     EEEEEEEEEEEEEEEEEEEEEENNNNNNNN        NNNNNNNNDDDDDDDDDDDDD
T:::::::::::::::::::::TH:::::::H     H:::::::HE::::::::::::::::::::E     E::::::::::::::::::::EN:::::::N       N::::::ND::::::::::::DDD
T:::::::::::::::::::::TH:::::::H     H:::::::HE::::::::::::::::::::E     E::::::::::::::::::::EN::::::::N      N::::::ND:::::::::::::::DD
T:::::TT:::::::TT:::::THH::::::H     H::::::HHEE::::::EEEEEEEEE::::E     EE::::::EEEEEEEEE::::EN:::::::::N     N::::::NDDD:::::DDDDD:::::D
TTTTTT  T:::::T  TTTTTT  H:::::H     H:::::H    E:::::E       EEEEEE       E:::::E       EEEEEEN::::::::::N    N::::::N  D:::::D    D:::::D
        T:::::T          H:::::H     H:::::H    E:::::E                    E:::::E             N:::::::::::N   N::::::N  D:::::D     D:::::D
        T:::::T          H::::::HHHHH::::::H    E::::::EEEEEEEEEE          E::::::EEEEEEEEEE   N:::::::N::::N  N::::::N  D:::::D     D:::::D
        T:::::T          H:::::::::::::::::H    E:::::::::::::::E          E:::::::::::::::E   N::::::N N::::N N::::::N  D:::::D     D:::::D
        T:::::T          H:::::::::::::::::H    E:::::::::::::::E          E:::::::::::::::E   N::::::N  N::::N:::::::N  D:::::D     D:::::D
        T:::::T          H::::::HHHHH::::::H    E::::::EEEEEEEEEE          E::::::EEEEEEEEEE   N::::::N   N:::::::::::N  D:::::D     D:::::D
        T:::::T          H:::::H     H:::::H    E:::::E                    E:::::E             N::::::N    N::::::::::N  D:::::D     D:::::D
        T:::::T          H:::::H     H:::::H    E:::::E       EEEEEE       E:::::E       EEEEEEN::::::N     N:::::::::N  D:::::D    D:::::D
      TT:::::::TT      HH::::::H     H::::::HHEE::::::EEEEEEEE:::::E     EE::::::EEEEEEEE:::::EN::::::N      N::::::::NDDD:::::DDDDD:::::D
      T:::::::::T      H:::::::H     H:::::::HE::::::::::::::::::::E     E::::::::::::::::::::EN::::::N       N:::::::ND:::::::::::::::DD
      T:::::::::T      H:::::::H     H:::::::HE::::::::::::::::::::E     E::::::::::::::::::::EN::::::N        N::::::ND::::::::::::DDD
      TTTTTTTTTTT      HHHHHHHHH     HHHHHHHHHEEEEEEEEEEEEEEEEEEEEEE     EEEEEEEEEEEEEEEEEEEEEENNNNNNNN         NNNNNNNDDDDDDDDDDDDD








");
        }

        public static void PlacementPion(bool ServeurQuiJoue, int CooX, int CooY)
        {
            Terrain.TableauGraines[CooX, CooY] = ServeurQuiJoue ? 's' : 'c';
            Terrain.TableauTerrain[CooX, CooY].owned = ServeurQuiJoue ? "s" : "c";
            IALogique.tableauGraines[CooX, CooY] = ServeurQuiJoue ? 's' : 'c';
            IALogique.tableauTerrain[CooX, CooY].owned = ServeurQuiJoue ? "s" : "c";
        }
    }
}

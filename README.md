# Bataille du café

Ce projet est le BOT ainsi que l'interface qui permet de jouer et observer une partie

Pour en savoir plus sur le pourquoi je vous invite a allez voir la partie context et le fichier Bataille_cafe_énoncer.pdf. Deux des exécutable fourni par notre profeseur sont présent, Phatt et Scrabb

## Installation

### Build le bot

Afin de compiler le projet il est nécessaire d'utiliser [dotnet core](https://dotnet.microsoft.com/download) 

Si l'ip ou le port uttiliser afin d'accéder aux bot différe de localhost:1213 veuillez modifier la ligne 11 de Play.cs

```c#
 public static class Play
    {
        public static connexion Connex = new connexion({L'adresse ip du serveur}, {Le port du serveur});
        public static decode deco;
```

Si vous changez l'addresse du serveur de l'interface veuillez changer

```c#
    public static class Interface
    {
        static SocketIO client = new SocketIO("https://myawesomecofee.herokuapp.com/");
        //Par
        static SocketIO client = new SocketIO({ l'addresse de votre serveur (suivie de son port ou non)});
        // Exemple en local
        static SocketIO client = new SocketIO("http://localhost:3000");
```

Puis il ne reste qu'a build

```bash
dotnet build
```

### Build l'interface

Afin de lancer l'interface développer en nodejs / express / socket.io / reactjs / mongo \
Rendez vous a la racine du projet

```bash
cd "Interface BETA"
```

Ici vous trouverez deux projet [npm](https://www.npmjs.com/)
#### API (Express / Socket.io / Mongodb)

Situez a la racine d'interface BETA

Afin d'initialiser et d'installer le projet :

```bash
npm install
```

Afin de configurer la partie [mongodb](https://www.mongodb.com/fr)(Mongo permet de creer une base en ligne gratuite) veuillez fournir un fichier a la racine de la partie interface nommer:

```json
{
    "url": {L'url de connection a la base mongodb},
    "dbname":{Le nom de la base}
}
```

Pour lancer le projet:
 
```bash
npm run start
```

### Interface Graphique (ReactJS PWA)

L'interface graphique est installable via des navigateur de derniére génération

Si vous voulez uttiliser un serveur autre que celui fourni modifiez la ligne ci dessous dans client/src/app.js et Display.js:

```js
var where = "https://myawesomecofee.herokuapp.com"
//par exemple
var where = "http://localhost:3000"
```

En cas de moficiation a l'inteface veuillez :

```bash
npm run build
```

## Contexte

Ce projet prend place en 2019/2020 au cours d'un projet tutoré. Le centre ? un serveur développé par notre Professeur Mr Clérentin. Notre objectif ? décoder une carte transmise via socket et jouer une partie toujours en socket contre ce serveur d'une bataille autour de la plantation de café.

## les régle :

- R1. La premiére pose est libre.
- R2. Il est interdit de placer une graine sur une unité de type Mer ou Forêt.
- R3. Un joueur doit poser une graine sur la même ligne ou la même colonne que la derniêre graine
placée.
- R4. Un planteur ne peut pas planter une graine sur la même parcelle que la derniére graine posée.
- R5. En cas de pose invalide, le planteur perd sa graine et son tour.
- R6. Si aucune des cases disponibles ne satisfait les régles R3 et R4, alors la partie est terminée.
- R7. Dés qu'un planteur a utilisé ses 28 graines, la partie est également terminée

[URL HEROKU](https://myawesomecofee.herokuapp.com/) [URL GIT](https://gitlab.com/cofee-team/my-awesomme-cofee)

## Un peu d'uml ?

### BOT

```plantuml
package Biblio <<Folder>>{
class Case{
    +int CooX { private set; get; }
    +int Cooy { private set; get; }

    +bool FORET
    +bool EAU

    +bool OUEST
    +bool SUD
    +bool EST
    +bool NORD

    +bool owned
    +bool treated
    
    +boot IsTERRE
    +bool IsTreatable
    +string IAM

    -int InitType(int )
    -void InitFrontiere(int )

    +ToString()
}

class connexion{
    -byte[] IP
    -IPAddress HOST
    -int port
    -Socket listener
    -int status
    -int Hostoustring

    +byte[] Connect()
    +void Disconect()
    +void Send(byte[] tosend)
    + byte[] Receive()
    -byte[] Intyobyte(int un,int deux, int trois,int quatre)
}

class Parcelle{
    +char id_parcelle
    +List<Case> cases {  get; private set; }

    +int Nbrunite
    +int PointServeur
    +int PointClient
    +string Whogotme
    +int Size_Parcelle
    +bool empty_owner

    +void Addcoordonnée(Case )
    +void Addlistcase(List<Case> )
    +Case findCase(int , int )
}
class Terrain{
    +Case[,] TableauTerrain {get }
    +char[,] TableauGraines {get}
    +List<Parcelle> list_parcelle { get; private set; }
    +char[,] Represymbolique { get; private set; }

    +void InitTerrain(string recu)
}
class  decode{
    + byte[] recu { get  protected set }
    +string type 

    +string decoded
    +string[] Splitstring

    +bool ContientEnco
    +bool ContientFini
    +bool ContientVali
    +bool ContientValiB
    +bool ContientInvaB 

    +int[] coo()
    +iint[] ResPartie()

    +byte[] EcnoreCOO( int[] )
    +byte[] Encode(string )
}

class Traitement_Parcelle{
    -case[] Terrain
    +char[,] Represymbolique { get; private set; }
    +List<Parcelle> Parcelles { get; private set; }

    +void BoucleTraitement(case[,])
    -List<CASE> Recurssion(int,int)

    +void Display()
}

Case o-- Terrain : contains
Case o-- Traitement_Parcelle : contains
Case o-- Parcelle : contains
decode o-- Terrain : contains
Parcelle o-- Traitement_Parcelle : contains
Parcelle o-- Terrain : contains
Terrain -- Traitement_Parcelle : Appelle

}
package PlayBot <<Folder>>{

    class IAlogique{
        +List<Parcelle> listparcelle
        +char[,] represymbolique { get; private set; }
        +char[,] tableauGraines
        +case[,] tableauTerrain { get; private set; }
        +int[,] powerCase { get; private set; }
        +int[] cooDernierPointPlacer { get; private set; }
        -Random random

        +void INIT ( char[,] , CASE[,] , List<Parcelle> )
        -void InitTab()

        +void BoucleTraitement(int[])
        -int PointCase(int , int)

        -Parcelle GetParcelle(int , int)
        -int ParcelleEmpty(int int)
        -int PointClientPointServeur(int int)
        -int PointClientGotAlfTheParcelle(int int)
        -int CasegotSiblingAround(int int)
        -bool PointServeurParcelleDefOwned(int int)
        -bool CaseDefOwned(int int)
        -bool PointONSameParcelle(int int)

        -List<CASE> GetListCaseTop()
        -int[] GetCaseWhenzero()
        -int[] GetcasefromList(List<CASE>)
        +int[] GetCase()
        +int[] PremierTour()
    }
    class Play{
        +connexion Connex
        +decode deco
        
        +Task StartPartie()
        
        +void PlayTurn()
        +bool TurnFromServeur()

        +void DisplayTurn(bool int[])
        +void DisplayPowerCase()
        +void DisplayOwned()
        +void DisplayScore()

        +void PlacementPion(bool int int)
    }

    class Interface{
        -SocketIO client
        +String ID { get; private set; }
        +bool status

        +Task INIT()

        +void SendInfoStartPartie()
        +void DisplayRepresSymbolique()

        +void DisplayScore(int[])
        +void WinLose(bool resultat)
        +void DisplayUnTour(bool int[])
        +void Valide(bool)
    }


}

Play -- Interface : Appelle
Play -- IAlogique : Appelle
Play -- Terrain : Appelle
Play o-- connexion: contains
Play o-- decode : contains
IAlogique o-- Parcelle : contains
IAlogique o-- Case : contains
IAlogique -- Terrain : Appelle
```
### Fonctionnement Partie

```plantuml

participant Serveur order 30
participant Bot order 10
activate Serveur
Bot -> Serveur: se connecte
activate Bot
Serveur -> Bot: Envoie la map
Bot-> Bot : Décode la map et découpe en parcelle
loop jusqu'a la fin de la partie
Bot->Serveur : Envoie un tour
Serveur -> Bot: Valide ou non
Serveur ->Bot: Coordonée ou FINI
Bot->Bot : Calcule de la case la plus intéressante
Serveur ->Bot: ENCO ou FINI
end
Serveur -> Bot : Envoie le score final
```

###  Fonctionnement inteface

```plantuml
actor User
activate User
User -> navigateur : Charge la page web
activate navigateur
navigateur -> API : Demande l'index
API -> navigateur : Renvoie la page React
navigateur -> navigateur : Effectue le rendu coté client
activate navigateur  #DarkSalmon
navigateur -> API : Effectue des requete de contenue a \nafficher telle que les score et les partie disponible
activate API
API -> mongodb : Effectue des requete pour le score\n et les partie dispo
activate mongodb
mongodb-> API : Renvoie les information demander
deactivate mongodb
API -> navigateur : Renvoie les contenue
deactivate API
navigateur -> navigateur : rendu de la page avec les info de l'api
deactivate navigateur
navigateur -> User : Affiche la page
deactivate navigateur

User -> navigateur : Afficher une partie
activate navigateur
navigateur -> API: Demande les informationde partie pour les afficher
activate API
API->API : Test si il posséde les information ou si il sont dans la bdd
activate API #DarkSalmon
API->mongodb : Cherche les information
mongodb-> API : Retourne les information
deactivate API
API-> navigateur : Renvoie les information de la partie si elle existe ou non
navigateur -> navigateur : Effectue le rendu
navigateur -> User : Renvoie la page

deactivate User
```

### Fonctionnement Communication API/Client

```plantuml
Client->ApiSocket :Notifie de son existance\n en ce connectent
activate Client
activate ApiSocket
ApiSocket -> Client : Retourne son ID de socket \n qui est unique et/identifiera la partie
Client->ApiSocket : Envoie la représentation Symbolique
ApiSocket -> ApiSocket : Ajoute a l'historique local
loop jusqu'a la fin de la partie
Client->ApiSocket : Envoie un tour
ApiSocket ->ApiSocket :ajoute le tour a l'historique local
end
Client->ApiSocket : Envoie le score de fin de partie
ApiSocket -> ApiSocket : ajoute le score a l'historique local
Client->ApiSocket : Envoie qui a gagner
deactivate Client
ApiSocket -> mongodb: Envoie l'historique local + Qui a gagner
deactivate ApiSocket
```